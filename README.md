# Máster en Ingeniería en Informática - UAL

## GGVD (Gestión de Grandes Volúmenes de datos)


El crecimiento en la producción de los datos de usuario, sensores, sistemas GPS, y demás, han disparado el tamaño y el tipo de los datos generados, los cuales además pueden ser de naturaleza no estructurada.Estos grandes volúmenes de datos suponen nuevos desafíos en términos de almacenamiento y procesamiento, por los que las técnicas tradicionales de gestión de datos no son aplicables directamente en este contexto. 

Bajo el término de NoSQL se encuentran los pricipales productos de bases de datos para tratar con este problema, como son las bases de datos orientadas a columnas, bases de datos clave-valor y bases de datos de documentos.En esta asignatura se estudian los conceptos fundamentales relacionados con NoSQL en el contexto de diferentes productos comerciales.

## Contenidos

* El fenónomeno NoSQL* Modelos de almacenamiento* Escalabilidad y Replicación* Desarrollo de aplicaciones con BD NoSQL
* Redis, MongoDB, Neo4j, ...

## Horario de clase y de tutorías

* Clases: Laboratorio Análisis y Desarrollo de Software - CITE III – (1.01)	* Martes de 18h a 20h (15 sesiones = 30 horas)* Profesor: Manuel Torres Gil
	* Tutorías: Miércoles y Jueves de 12h a 15h
	* Despacho: 2.19.5 CITE III (2a planta)
	* email: [mtorres@ual.es](mailto:mtorres@ual.es)	* Twitter: [@ualmtorres](https://twitter.com/ualmtorres)	* Canal Slack Asignatura: [#ggvd2015](https://ualinformatica.slack.com/messages/ggvd2015/)

## Cómo seguir la asignatura

* Material disponible en
	* Aula Virtual	* [Repo GitLab](http://192.168.60.180/mtorres/ggvd2015/tree/master) (accesible sólo mediante VPN UAL)
* Clases participativas* Contenido práctico* Planteamiento de retos/desafíos
	* Redis	* MongoDB
	* Neo4j

## Cómo superar la asignatura

* Se valora la asistencia y participación* Planteamiento de retos/desafíos
	* Redis
	* MongoDB
	* Neo4j