# GGVD - Sesión 01
* Presentación de la asignatura
	* Contenidos
	* Horario de clase y de tutorías
	* Cómo seguir la asignatura
	* Cómo superar la asignatura* [El fenómeno NoSQL](../master/Presentaciones/01 El fenomeno NoSQL.pdf)

## Contenidos

* El fenónomeno NoSQL* Modelos de almacenamiento* Escalabilidad y Replicación* Desarrollo de aplicaciones con BD NoSQL
* Redis, MongoDB, Neo4j, ...

## Horario de clase y de tutorías

* Clases: Laboratorio Análisis y Desarrollo de Software - CITE III – (1.01)	* Martes de 18h a 20h (15 sesiones = 30 horas)* Profesor: Manuel Torres Gil
	* Tutorías: Miércoles y Jueves de 12h a 15h
	* Despacho: 2.19.5 CITE III (2a planta)
	* email: [mtorres@ual.es](mailto:mtorres@ual.es)	* Twitter: [@ualmtorres](https://twitter.com/ualmtorres)	* Canal Slack Asignatura: [#ggvd2015](https://ualinformatica.slack.com/messages/ggvd2015/)

## Cómo seguir la asignatura

* Material disponible en
	* Aula Virtual	* [Repo GitLab](http://192.168.60.180/mtorres/ggvd2015/tree/master) (accesible sólo mediante VPN UAL)
* Clases participativas* Contenido práctico* Planteamiento de retos/desafíos
	* Redis	* MongoDB
	* Neo4j

## Cómo superar la asignatura

* Se valora la asistencia y participación* Planteamiento de retos/desafíos
	* Redis
	* MongoDB
	* Neo4j
