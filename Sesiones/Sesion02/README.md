# GGVD - Sesión 02

## Modelos de almacenamiento en el ámbito NoSQL

* Introducción a los principales Modelos de datos en BD NoSQL
	* Bases de datos Clave-valor
	* Bases de datos orientadas a Documentos
	* Bases de datos de Familias de columnas
	* Bases de datos orientadas a grafos
* Agregados. Ventajas e inconvenientes
* Panorama actual* [Modelos de almacenamiento NoSQL](../master/Presentaciones/02 Modelos de Almacenamiento NoSQL.pdf)

