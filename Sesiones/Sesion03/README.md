# GGVD - Sesión 03

## Redis

* Instalación
* Interacción básica con Redis
	* Operaciones elementales
	* Operaciones sobre listas
	* Operaciones sobre conjuntos
	* Operaciones sobre conjuntos ordenados
	* Operaciones sobre hashes
* Otras opciones interesantes en Redis
	* Expiración de claves
	* Espacios de nombres
	* Publicación subscripción
	* Persistencia en Redis
	* Replicación
* [Redis](../master/Presentaciones/03 Redis.pdf)

