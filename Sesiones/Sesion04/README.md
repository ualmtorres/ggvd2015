# GGVD - Sesión 04

## Interacción con Redis desde Java y PHP* [Interacción básica desde Java](http://ualmtorres.github.io/howtos/RedisJava/)* [Interacción básica desde PHP](http://ualmtorres.github.io/howtos/RedisPHP/PHPRedisBasico/)* [Desarrollo de una API REST en PHP usando Phalcon](http://ualmtorres.github.io/howtos/RedisPHP/PhalconRedisAPIREST/)Si no tienes instalado el driver PhpRedis sigue este [tutorial para instalar PhpRedis](http://ualmtorres.github.io/howtos/RedisPHP/InstalacionPHPRedis/)

### Código disponible

* [Repositorio GitHub con ejemplo interacción básica con Redis desde Java](https://github.com/ualmtorres/RedisJava)
* [Repositorio GitHub con ejemplo API REST básica en PHP usando Phalcon](https://github.com/ualmtorres/HelloRESTAPIPhalcon) 
* [Repositorio GitHub con ejemplo API REST para Redis en PHP usando Phalcon](https://github.com/ualmtorres/HelloRedisRESTAPIPhalcon) 