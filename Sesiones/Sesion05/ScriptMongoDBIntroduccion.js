// Distinción entre mayúsculas y minúsculas
{"saludo": "Hola a todos", "Saludo": "Hello world"}

// Documento no legal si hay claves duplicadas
{"nota": 10, "nota": 8}

/*
 * Shell de MongoDB
 */

dias = 365

dias / 30

function factorial (n) {
	if (n <= 1) return 1;
	return n * factorial (n - 1);
}

factorial (5)

/*
 * insert() y find()
 */ 
doc = {name: "Juan", age: 40, profesion: "fontanero"}
db
db.people.insert(doc)
db.people.find()
db.people.insert({name: "Luisa", age: 45, profesion: "cocinera"})
db.people.insert({name: "Susana", age: 19, hobbies: ["futbol", "cine", "teatro"]})
db.people.insert({name:"Antonio", age:24, hobbies:["futbol", "tenis"]})
db.people.find()

/*
* find() con parámetros
*/

db.people.find({_id: ObjectId("53f066ff17e2b0c98e498cd3")})
db.people.find({_id: ObjectId("53f066ff17e2b0c98e498cd3")}, {name:true})
db.people.find({_id: ObjectId("53f066ff17e2b0c98e498cd3")}, {name:false})
db.people.find({profesion: "fontanero"}, {_id:false})

/*
 * $exists() para determinar si existe un campo 
 */

db.people.find({hobbies:{$exists:true}}).pretty()

/*
* Ejecución de JavaScript para poblar una colección 
*/

var types = ["examen", "trabajo automono", "prueba de nivel"];
for (student_id = 0; student_id < 1000; student_id++) { 
	for (type = 0; type < 3; type++) { 
		var r = {student_id:student_id, type:types[type], score:Math.round(Math.random()*100)}; 
		db.scores.insert(r) 
	}
}

db.scores.find();
db.scores.find().pretty();

/*
* find() con varios criterios
*/

db.scores.find({student_id: 19, type: "examen"}).pretty()
db.scores.find({$or:[{student_id:19}, {student_id: 20}], type:"examen"}).pretty()
db.scores.find({student_id: 19, type: "examen"}, {_id: false}).pretty()

/*
 * findOne() para recuperar sólo un documento
 */

db.scores.findOne()

/*
 * Documentos embebidos
 */

{
	"nombre": "Juan Pérez",
	"direccion":	{
		"calle":	"Paseo de Almería, 1",
		"cp":		"04001"
		"ciudad":	"Almería"
					}
}

/*
* Operadores de comparación
*/

db.scores.find({"score":{"$gt":98}})
db.scores.find({"score":{"$gt":98}, "type":"examen"})
db.scores.find({"score":{"$gt":98, "$lte":99}, "type":"examen"})

/*
* Uso de arrays
*/

db.people.find({hobbies:"futbol"}).pretty()
db.people.find({hobbies:["futbol", "cine"]})
db.people.find({$or:[{hobbies:"futbol"}, {hobbies:"cine"}]}).pretty()
db.people.find({hobbies:{$in:["futbol", "cine"]}}).pretty()
db.people.find({hobbies:{$all:["futbol", "cine"]}}).pretty()

/*
 * Búsqueda de subelementos con notación punto 
*/

db.countries.insert({
	_id : "us",
	name : "United States", 
	exports : {
		foods : [
	         { name : "bacon", tasty : true }, 
	         { name : "burgers" }
         ]
	}
})

db.countries.insert({
	_id : "ca",
	name : "Canada", 
	exports : {
		foods : [
	         { name : "bacon", tasty : false },
	         { name : "syrup", tasty : true } 
         ]
	} 
})

db.countries.insert({
	_id : "mx",
	name : "Mexico", 
	exports : {
		foods : [
	         {name : "salsa", tasty : true, condiment : true}
         ]
	}
})

db.countries.find({"exports.foods.name":"bacon"}).pretty()
db.countries.find({"exports.foods.name":"bacon"}, {name:true, _id:false})

/*
 * $elemMatch para comparar subelementos. NO EQUIVALE A UN AND
 */

db.countries.find({"exports.foods.name":"bacon", "exports.foods.tasty":true}, {name:true, _id:false})
db.countries.find({"exports.foods": {
	$elemMatch: {
         "name": "bacon",
         "tasty":true
	}
}}, {name:true, _id:false})

/*
* Convertir campos en arrays y añadir un valor
*/

db.people.update({name:"Luisa"}, {$set:{profesion:["modista"]}})
db.people.update({name:"Luisa"}, {$addToSet:{profesion:"cocinera"}})

/*
* Añadir elementos a un array - $push() y $pushAll()
*/

db.people.update({name: "Luisa"}, {$push:{profesion: "peluquera"}})
db.people.update({name: "Luisa"}, {$pushAll:{profesion: ["cajera", "administrativa"]}})

/*
* Sacar elementos de un array - $pull() y $pullAll()
*/

db.people.update({name: "Luisa"}, {$pull:{profesion: "peluquera"}})
db.people.update({name: "Luisa"}, {$pullAll:{profesion: ["cajera", "administrativa"]}})

/*
 * Sacar el primer (-1) o el último (1) elemento de un array - $pop()  
 */

db.people.update({name: "Luisa"}, {$pop:{profesion: 1}})

/*
* Actualizar varios documentos a la vez
*/

db.people.update({}, {$set:{livesIn: "Almeria"}}, {multi:true})

/*
* Incrementar valores numéricos – $inc()
*/

db.people.update({name: "Luisa"}, {$inc: {age: 5}})

/*
 * Eliminación de campos - $unset
 */

db.people.update({profesion: "antenista"}, {$unset:profesion})

/*
 * Eliminación de documentos - remove()
 */

db.people.remove({profesion: "antenista"})

/*
 * Ordenación - sort()
 */

 db.people.find({}, {_id:false}).sort({name:1})
 
/*
* limit() y skip()
*/

db.people.find({}, {_id:false}).sort({name:1}).limit(2)
db.people.find({}, {_id:false}).sort({name:1}).skip(1).limit(2)

/*
* upsert: Inserta si no hay nada que actualizar
*/

db.people.update({livesIn: "Granada"}, {$set: {name: "Francisca", profesion: ["comercial"], age:52}}, {upsert:true})

/* 
 * id compuesto
 * El _id no tiene por que ser siempre un valor numérico ni un valor simple
 */

db.prueba.insert(
		{
			'_id': {'nombre':' Manuel', 'apellidos': 'Torres Gil'}, 
			'profesion': 'profesor'
		}
);

db.prueba.find();

/*
 * Pero dará error si lo volvemos a introducir porque intenta crear una 
 * entrada duplicada en el índice
 */
db.prueba.insert(
		{
			'_id': {'nombre':' Manuel', 'apellidos': 'Torres Gil'}, 
			'profesion': 'profesor'
		}
);

/*
 * Búsquedas aproximadas
 */

db.users.insert({name: 'paulo'})
db.users.insert({name: 'patric'})
db.users.insert({name: 'pedro'})

db.users.find({name: /a/})  //like '%a%'
// out: paulo, patric

db.users.find({name: /^pa/}) //like 'pa%' 
// out: paulo, patric

db.users.find({name: /ro$/}) //like '%ro'
// out: pedro

db.users.find({name: /RIC$/i}) //like '%ric' Case insensitive
// out: patric

db.users.find({name: /^p.*o$/}) //like 'p%o'
// out: paulo, pedro

db.users.find({name: /(^pau)|(^pat)/}) //like 'pau%' or like 'pat%'
// out: paulo, patric

/*
 * El método count()
 */

// Cuántos documentos almacena scores
db.scores.find().count()
db.scores.count()

// Cuántos documentos hay de tipo examen con nota superior a 95
db.scores.find({type: 'examen', score:{$gt: 95}}).count()
db.scores.count({type: 'examen', score:{$gt: 95}})


/*
* distinct()
*/

// Profesiones diferentes almacenadas
db.people.distinct('profesion')

// Profesiones diferentes de las personas que viven en Almería
db.people.distinct('profesion', {livesIn: 'Almeria'})
