/*
 * Consulta de índices disponibles
 */

use school
db.scores.getIndexes()

/*
 * Lento porque no esta indexado y hay que recorrerlo todo
 */
db.scores.find({'student_id': 1});

/*
 * Rapido porque lo encuentra rapido al hacer el reccorido secuencial
 */
db.scores.findOne({'student_id': 1});

/*
 * Lento porque tarda en encontrarlo al hacer el recorrido secuencial
 */
db.zips.findOne({'student_id': 1000000});

/*
 * Lento porque no esta indexado y tiene que recorrerlo todo
 */
db.zips.find({'student_id': 1000000});


/*
* Creación de índices
*/

db.scores.find({student_id:999}).explain()
db.scores.ensureIndex({student_id:1}) 
db.scores.ensureIndex({student_id:1, type:1}) 
db.scores.find({student_id:999}).explain()

/*
* Eliminación de índices
*/

db.scores.dropIndex({student_id:1})
db.scores.dropIndex({student_id:1, type:1})
db.scores.getIndexes()

/*
 * Indices compuestos y arrays
 */

db.multikey.ensureIndex({a:1, b:1})
db.multikey.insert({a:1, b:1})
db.multikey.insert({a:[1, 2, 3], b:1})
db.multikey.insert({a:1, b:[1, 2, 3]})
db.multikey.insert({a:[1, 2, 3], b:[1, 2, 3]})

/*
* Indices definidos a cualquier nivel de documento
*/ 

db.people.ensureIndex({'addresses.tag':1});
db.people.ensureIndex({'addresses.phone':1});

db.people.getIndexes();

/*
* Indices sin duplicados
*/

db.stuff.insert({cosa: 'ordenador'})
db.stuff.insert({cosa: 'puerta'})
 
db.stuff.ensureIndex({cosa: 1}, {unique:true})

db.stuff.getIndexes()

/*
* Creación de índices con eliminación de duplicados
*/

db.stuff.drop()
db.stuff.insert({cosa: 'ordenador', marca: 'hp'})
db.stuff.insert({cosa: 'ordenador', marca: 'toshiba'})
db.stuff.insert({cosa: 'puerta', marca: 'acme'})

db.stuff.ensureIndex({cosa: 1}, {unique:true, dropDups: true})

/*
* Indices dispersos (Admiten valores nulos)
*/ 

db.stuff.insert({cosa: 'coche', color:'azul'})
db.stuff.insert({cosa: 'moto', color:'verde'})

db.stuff.ensureIndex({color: 1}, {unique: true, sparse: true})

/*
* Documentos con NULL en clave de indice disperso no son recuperados al ordenar
*/

db.stuff.find({}, {_id: 0})
 
db.stuff.find({}, {_id: 0}).sort({color: 1})


/*
 * Uso de índices por una consulta
 */

db.foo.ensureIndex({a:1, b:1, c:1})

// Consultas que usarán el índice
db.foo.find({a:3})
db.foo.find({c:1}).sort({a:1, b:1})

// Consultas que no usarán el índice
db.foo.find({b:3, c:4})
db.foo.find({c:1}).sort({a:-1, b:1})

/*
* Indices geoespaciales
*/

use test;
db.places.insert({name: 'Juani', type: 'Peluquería', location: [40, 41.22]});
db.places.insert({name: 'Andrea', type: 'Peluquería', location: [39.18, 40.12]});
db.places.insert({name: '3 llaves', type: 'Menaje', location: [40.232, -74.343]});
db.places.insert({name: 'Supertodo', type: 'Alimentación', location: [41.232, -75.343]});

// Creación del índice 2D
db.places.ensureIndex({location: '2d', type: 1});

// Documentos más cercanos a [40, 40]
db.places.find({type: 'Peluquería', location:{$near: [40, 40]}}, {_id:0});

//Documento más cercano a [40, 40]
db.places.find({type: 'Peluquería', location:{$near:[40, 40]}}, {_id:0}).limit(1)

/*
 * Registro de consultas lentas. El profiler
 */

// Consulta del estado del Profiler
db.getProfilingLevel()
db.getProfilingStatus()

// Activar el Profiler para registrar consultas que tarden más de 4 ms
db.setProfilingLevel(1, 4)

// Desactivación del Profiler
db.setProfilingLevel(0)