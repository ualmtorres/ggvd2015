# GGVD - Sesión 07

## MongoDB – Framework de Agregación* Introducción
* Funciones en el `$group`
* Transformaciones con `$project`
* Ordenación y limitación de resultados con `$order, $skip, $limit`
* Filtrador previo y a posteriori con `$match`
* Desanidado con `$unwind`[MongoDB – Agregación](../master/Presentaciones/04 MongoDB - Agregacion.pdf)
