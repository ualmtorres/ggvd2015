# GGVD - Sesión 10

## MongoDB desde aplicaciones Java

* Agregación

## MongoDB desde aplicaciones PHP

* API PHP para MongoDB (http://php.net/mongo)
* Configuración y conexión
* Operaciones básicas
* Agregación

[MongoDB – Java (Slides)](http://slides.com/manolotorres/m)
[Usando MongoDB con Java](http://ualmtorres.github.io/howtos/MongoDBJava/)
[Usando MongoDB con PHP](http://ualmtorres.github.io/howtos/MongoDBPHP/)
[Ejemplo Blog](http://192.168.60.157/MongoDBBlog)