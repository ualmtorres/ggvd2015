# GGVD - Sesión 13

## Neo4j desde aplicaciones PHP

* Configuración y conexión
* Operaciones básicas

* [NeoClient: Driver PHP para Neo4j (Slides)](http://slides.com/manolotorres/neoclient)
* [Usando Neo4j con PHP](http://ualmtorres.github.io/howtos/Neo4jPHP/)
* [Ejemplos NeoClient](https://github.com/ualmtorres/Neo4jPHP)
* [Ejemplo Movies](http://192.168.60.157/Neo4jMovies)